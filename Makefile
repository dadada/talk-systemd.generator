.PHONY: all clean html

all: html

clean:
	rm -f talk.html

html: talk.html
talk.html: talk.md
	pandoc -t revealjs -s -o $@ $< -V revealjs-url=https://revealjs.com

